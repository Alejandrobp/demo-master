package com.example.demo;

public class DataBean {

	private Double quantity = 0.0;
	private Double total = 0.0;
	private Double factor = 0.0;
	private Double suma = 0.0;

	public DataBean() {
	}

	public DataBean(Double quantity, Double factor) {
		this.quantity = quantity;
		this.factor = factor;
		this.total = multiplicar();
		this.total = dividir();
		//Algun comentario
		//Otro comentario
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getFactor() {
		return factor;
	}

	public void setFactor(Double factor) {
		this.factor = factor;
	}
	
	public Double multiplicar() {
		return quantity * factor;
	}
	
	public Double dividir() {
		return quantity / factor;
	}
}
